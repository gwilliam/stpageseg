import streamlit as st

from core.Page import Page

class SubPage1(Page):
    def __init__(self, state):
        super().__init__("SubPage1", "First SubPage", state)

    def main(self):
        super().main()

        st.write("sub page 1 content")
